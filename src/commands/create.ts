import inquirer from 'inquirer'
import request from 'request'
import AdmZip from 'adm-zip'
import path from 'path'
import fs from 'fs'
import { exec } from 'child_process'
import { getProjectDir, createFoldersIfNotExist, cwd, isFile, dbDependencies } from '../common'

const urls = {
    default: 'https://expressts-web-api.s3-ap-southeast-1.amazonaws.com/1.0.0/default.zip'
}

function writeFile(filePath, data, cb) {
    fs.writeFile(filePath, data, { encoding: 'utf-8' }, cb)
}

async function askQuestions(options) {
    const defaultOptions = {
        ...options,
        name: 'expressts-web-api',
        db: 'mysql'
    }
    if (options.default) {
        return defaultOptions
    }

    const questions = []
    if (!options.name) questions.push({
        name: 'name',
        message: 'What is your project name?',
        default: defaultOptions.name
    })

    if (!options.db) questions.push({
        type: 'list',
        name: 'db',
        message: 'Which database would you like to use?',
        choices: ['mysql', 'mariadb', 'postgres', 'mssql'],
        default: defaultOptions.db
    })

    const answers = await inquirer.prompt(questions)

    return {
        ...options,
        db: options.db || answers.db,
        name: options.name || answers.name
    }
}

function rewritePackage(data, options) {
    data = data.replace(/{packageName}/g, options.name)
    return ['package.json', data]
}

function rewriteEnv(data, options) {
    data = data.replace(/{db}/g, options.db)
    return ['.env', data]
}

function createTemplate(options) {
    return new Promise((rs, rj) => {
        let url = urls.default
        if (!url) return rj(new Error('Cannot find template'));

        request({
            url,
            encoding: null
        }, (err, res, body) => {
            if (err) throw err
            const zip = new AdmZip(body)
            const zipEntries = zip.getEntries()
            const projectDir = getProjectDir(options.name)

            var pendings = zipEntries.filter(entry => isFile(entry.entryName)).map(entry => entry.entryName)

            const errors = []

            const dequeue = (entryName) => {
                return () => {
                    pendings = pendings.filter(name => name !== entryName)
                    if (!pendings.length) {
                        if (errors.length) {
                            errors.forEach(err => console.error(err))
                            return rj()
                        }
                        rs()
                    }
                }
            }


            zipEntries.forEach((entry) => {
                try {
                    if (!isFile(entry.entryName)) return

                    let entryData = zip.readAsText(entry)
                    createFoldersIfNotExist(`${options.name}/${entry.entryName}`, cwd)

                    let entryName = entry.entryName

                    if (entryName.match(/package.pattern.json/g)) {
                        [entryName, entryData] = rewritePackage(entryData, options)
                    }
                    if (entryName.match(/.env.pattern/g)) {
                        [entryName, entryData] = rewriteEnv(entryData, options)
                    }
                    if (entryName.match(/.gitignore.pattern/g)) {
                        entryName = '.gitignore'
                    }

                    let filePath = path.resolve(projectDir, entryName)
                    console.log('Creating file: ', entryName)
                    writeFile(filePath, entryData, dequeue(entry.entryName))
                } catch (error) {
                    errors.push(error)
                }
            });
        })
    })
}

export default async function create(options) {
    console.info('Creating your express web api...')
    options = await askQuestions(options)

    try {
        await createTemplate(options)
    } catch (error) {
        console.info('Process exited on error')
        console.error(error)
        process.exit(1)
    }

    const installDependencies = () => {
        return new Promise((rs, rj) => {
            console.log('Installing dependencies...')
            let projectDir = getProjectDir(options.name)
            exec(`npm i`, {
                cwd: projectDir
            })
                .on('close', rs)
                .on('error', rj)
        })
    }

    const installDbDependencies = () => {
        return new Promise((rs, rj) => {
            console.log('Installing database dependencies...')
            let projectDir = getProjectDir(options.name)
            let dependencies = dbDependencies[options.db]
            if (!dependencies || !dependencies.length) return rs()
            const command = `npm i -s ${dependencies.join(' ')}`
            exec(command, {
                cwd: projectDir
            })
                .on('close', rs)
                .on('error', rj)
        })
    }

    try {
        await installDependencies()
        if (options.db) await installDbDependencies()
        console.info('Done! Happy hacking!')
    } catch (error) {
        console.error(error)
        process.exit(1)
    }
}